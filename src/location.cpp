/*
 * location.cpp
 *
 *  Created on: 19 Feb 2018
 *      Author: dylan
 */

#include "../headers/location.hpp"

Point Cluster::resolveLocation()
{
	float mean[3] = {0,0,0};
	for(size_t i = 0; i < points.size(); i++) {
		mean[0] += points[i].x;
		mean[1] += points[i].y;
		mean[2] += points[i].z;
	}
	mean[0] /= points.size(), mean[1] /= points.size(), mean[2] /= points.size();

	Point mean_point(mean[0], mean[1], mean[2], points[0].t);
	mean_point.c_size = points.size();

	for(size_t i = 0; i < 3; i++) {
		for(size_t j = 0; j < 3; j++) {
			mean_point.covariance[i][j] = 0.0;
			for(size_t k = 0; k < points.size(); k++) {
				mean_point.covariance[i][j] += (mean[i]-points[k].coord(i)) *
						                       (mean[j]-points[k].coord(j));
			}
			mean_point.covariance[i][j] /= points.size()-1;
		}
	}

	return mean_point;
}



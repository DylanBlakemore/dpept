/*
 * measurement.cpp
 *
 *  Created on: 22 Feb 2018
 *      Author: dylan
 */

#include "../headers/measurement.hpp"

Measurement::Measurement()
{
	init();
}

Measurement::Measurement(float x_, float y_, float z_, float t_in,
						 size_t csize, const vector<float>& cov)
{
	init();

	x.add(1,1,x_);
	x.add(2,1,y_);
	x.add(3,1,z_);

	for(size_t i = 0; i < 3; i++) {
		for(size_t j = 0; j < 3; j++) {
			R.add(i+1, j+1, cov[i*3 + j]);
		}
	}

	t = t_in;
	cluster_size = csize;
}

void Measurement::init()
{
	x = Matrix(3,1);
	R = Matrix(3,3);
	t = 0;
	cluster_size = 0;
}



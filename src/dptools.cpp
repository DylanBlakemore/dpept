/*
 * dptools.cpp
 *
 *  Created on: 17 Jan 2018
 *      Author: dylan
 */

#include "../headers/dptools.hpp"

Settings::Settings(const string& f_path)
{
	config_path = f_path;
	// ------------------------------------------------------------
	// Read in values from configuration file.
	// ------------------------------------------------------------
	INIReader reader(f_path);

	input_file    = reader.Get("Input", "input_file", "UNKNOWN");
	output_folder = reader.Get("Input", "output_folder", "UNKNOWN");
	track_only    = (bool)reader.GetInteger("Input", "track_only", 0);

	n_tracers  = reader.GetInteger("Input", "n_tracers", 1);
	n_lines    = reader.GetInteger("Input", "n_lines", 100);
	n_batches  = reader.GetInteger("Input", "n_batches", -1);
	n_frames   = reader.GetInteger("Input", "n_frames", 100);
	start_time = reader.GetInteger("Input", "start_time", 0);
	window	   = reader.GetInteger("Input", "window", 0);
	n_cores    = reader.GetInteger("Input", "n_cores", 1);

	min_decay     = (float)reader.GetReal("Triangulation", "min_decay", 10);
	disc_length   = (float)reader.GetReal("Triangulation", "disc_length", 4.0);
	lof_frac      = (float)reader.GetReal("Triangulation", "lof_frac", 0.5);
	vol_frac      = (float)reader.GetReal("Triangulation", "vol_frac", 0.5);
	dbscan_eps    = (float)reader.GetReal("Triangulation", "dbscan_eps", 5.0);
	lof_k         = reader.GetInteger("Triangulation", "lof_k", 5);
	dbscan_minpts = reader.GetInteger("Triangulation", "dbscan_minpts", 5);

	error_sigma = (float)reader.GetReal("Tracking", "error_sigma", 1);
	search_rad  = (float)reader.GetReal("Tracking", "search_rad", 10);
	max_dt      = (float)reader.GetReal("Tracking", "max_dt", 10);
	max_overlap = (float)reader.GetReal("Tracking", "max_overlap", 20);
	min_entries = reader.GetInteger("Tracking", "min_entries", 50);
	use_acc		= (bool)reader.GetInteger("Tracking", "use_acc", 0);
}

float sqrdist(const float* A, const float* B)
{
	return (A[0]-B[0])*(A[0]-B[0]) +
		   (A[1]-B[1])*(A[1]-B[1]) +
		   (A[2]-B[2])*(A[2]-B[2]);
}

float tetVolume(const FADE3D::Tet3* tet)
{
	FADE3D::Point3 *p0, *p1, *p2, *p3;

	tet->getCorners(p0, p1, p2, p3);

	float vol;
	FADE3D::Vector3 a = *p1 - *p0;
	FADE3D::Vector3 u = *p2 - *p0;
	FADE3D::Vector3 v = *p3 - *p0;

	/*
	 * Volume = |a*(b x c)|/6
	 */
	// Cross product
	FADE3D::Vector3 cross = FADE3D::Vector3(u.y()*v.z() - u.z()*v.y(),
											u.z()*v.x() - u.x()*v.z(),
											u.x()*v.y() - u.y()*v.x());
	// Scalar product and division
	vol = abs(a*cross)/6;
	return vol;
}


int printLocations(const Location& loc, const Settings& settings, char delim)
{
	string folder = settings.output_folder;
	ofstream l_file, c_file;
	if(loc.points.size() == 0)
		return 1;

	string location_path = folder + "/locations.dsv";
	l_file.open(location_path.c_str(), std::ios_base::app);
	if(!l_file.is_open())
		return 0;

	// Write the covariance matrix values
	string covariance_path = folder + "/covariance.dsv";
	c_file.open(covariance_path.c_str(), std::ios_base::app);

	if(!c_file.is_open())
		return 0;

	for(size_t i=0; i<loc.points.size(); i++) {
		Point P_i = loc.points[i];
		if(P_i.c_size < settings.dbscan_minpts)continue;
		l_file << '\n';
		l_file << fixed << setprecision(2) << P_i.x << delim;
		l_file << fixed << setprecision(2) << P_i.y << delim;
		l_file << fixed << setprecision(2) << P_i.z << delim;
		l_file << fixed << setprecision(2) << P_i.t << delim;
		l_file << P_i.c_size << delim;
		l_file << loc.id;

		c_file << '\n';
		for(size_t i = 0; i < 3; i++) {
			for(size_t j = 0; j < 3; j++) {
				float covariance = P_i.covariance[i][j];
				c_file << fixed << setprecision(2) << covariance;
				if(i*j < 4)
					c_file << delim;
			}
		}
	}

	l_file.close();
	c_file.close();

	return 1;
}

int printTracks(const Track& track, size_t id, const Settings& settings, char delim)
{
	string filename = settings.output_folder + "/tracks.dsv";
	ofstream t_file;
	t_file.open(filename.c_str(), std::ios_base::app);
	if(!t_file.is_open())
		return 0;

	size_t xyz_position[3];
	if(settings.use_acc) {
		xyz_position[0] = 1;
		xyz_position[1] = 4;
		xyz_position[2] = 7;
	} else {
		xyz_position[0] = 1;
		xyz_position[1] = 3;
		xyz_position[2] = 5;
	}

	size_t n_states = track.states.size();
	for(size_t i = 0; i < n_states; i++) {
		State i_state = track.states[i];
		t_file << '\n';
		// X, Y, Z coords
		t_file << fixed << setprecision(2) << i_state.x_hat.getNumber(xyz_position[0],1) << delim;
		t_file << fixed << setprecision(2) << i_state.x_hat.getNumber(xyz_position[1],1) << delim;
		t_file << fixed << setprecision(2) << i_state.x_hat.getNumber(xyz_position[2],1) << delim;
		t_file << fixed << setprecision(2) << i_state.t << delim;
		t_file << id << delim;
		t_file << i_state.cluster_size;
	}

	t_file.close();
	return 1;
}

int createFile(const string& path, const string& header)
{
	ofstream ofile;
	ofile.open(path.c_str());
	ofile << header;
	ofile.close();

	return 1;
}

int createOutputFiles(Settings& settings)
{
	string tracks_fname = settings.output_folder + "/tracks.dsv";
	string tracks_header = "x\ty\tz\tt\tID\tN";
	createFile(tracks_fname, tracks_header);

	if(settings.track_only)
		return 1;

	string location_fname = settings.output_folder + "/locations.dsv";
	string location_header = "x\ty\tz\tt\tc\tid";
	createFile(location_fname, location_header);

	string covariance_fname = settings.output_folder + "/covariance.dsv";
	string covariance_header = "xx\txy\txz\tyx\tyy\tyz\tzx\tzy\tzz";
	createFile(covariance_fname, covariance_header);

	string log_fname = settings.output_folder + "/log.txt";
	string log_header = "Batch\tClusters\tTime";

	return 1;
}

int createOutputFolder(Settings& settings)
{
	if(settings.track_only)
		return 1;

	int counter = 1;
	string original_folder = settings.output_folder;
	while(mkdir(settings.output_folder.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH))
	{
		settings.output_folder = original_folder + "_" + std::to_string(counter);
		counter++;
	}
	return 1;
}

float measurementDistance(Measurement& m1, Measurement& m2)
{
	float distance = 0;
	float x1[3];
	float x2[3];

	for(size_t i = 0; i < 3; i++) {
		x1[i] = m1.x.getNumber(i+1,1);
		x2[i] = m2.x.getNumber(i+1,1);
	}

	distance = sqrt(sqrdist(x1,x2));

	return distance;
}

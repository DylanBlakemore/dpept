/*
 * pointset.cpp
 *
 *  Created on: 08 Feb 2018
 *      Author: dylan
 */


#include "../headers/pointset.hpp"

vector<Point> PointSet::cluster(size_t k, float eps)
{
	DBSCAN dbscan(points, k, eps);
	dbscan.run();
	float t = points[0].t;

	int n_clusters = dbscan.clusterIdx + 1;

	vector<Cluster> clusters(n_clusters);
	vector<Point> locs(n_clusters, Point(0,0,0,t));

	// ---------------------------------------------------------------------------
	// For each point, add it element-wise to the relevant cluster (determined
	// by the cluster ID. c_ID = -1 implies noise.
	// ---------------------------------------------------------------------------
	for(size_t i=0; i<points.size(); i++) {
		int c = dbscan.points[i].cluster;
		if(c == -1 || c == -2) continue;

		clusters[c].points.push_back(points[i]);
	}
	// ---------------------------------------------------------------------------
	// Divide by cluster sizes to get averages.
	// ---------------------------------------------------------------------------
	for(int c=0; c<n_clusters; c++) {
		locs[c] = clusters[c].resolveLocation();
	}

	return locs;
}

void PointSet::cleanse(size_t k, float lof_frac, float vol_frac)
{
	// -----------------------------------------------------------
	// Get the lof values for all the points.
	// -----------------------------------------------------------
	LOF lof_calc(&points);
	vector<float> lofs = lof_calc.calculate(k);
	// -----------------------------------------------------------
	// Determine the number of points to keep based on the fraction
	// of LOFs considered noise, and remove points with LOFs higher
	// than the associated value.
	// -----------------------------------------------------------
	size_t N_lof = (size_t)(lof_frac * (float)points.size());
	removeLarge(lofs, N_lof);

	// -------------------------------------------------------------
	// Determine the number of points to keep based on the Delaunay
	// volumes. Remove points with volumes greater than the maximum.
	//
	// In future versions this may be changed to incorporate an
	// Otsu method for bimodal distribution segregation.
	// -------------------------------------------------------------
	vector<float> vols(points.size());
	for(size_t v=0; v<vols.size(); v++)
		vols[v] = points[v].vol;

	size_t N_vol = (size_t)(vol_frac * (float)points.size());
	removeLarge(vols, N_vol);
}

void PointSet::removeLarge(const vector<float>& metric, size_t N)
{

	vector<Point> points_tmp(N);
	size_t i,j;

	float max_val = getKthSmallest(metric, N);

	i = 0;
	for(j=0; j<points.size(); j++) {
		if(i >= N)break;
		if(metric[j] <= max_val) {
			points_tmp[i] = points[j];
			i++;
		}
	}
	points = points_tmp;;
}

void PointSet::insert(Point p)
{
	if(n < points.size()) {
		points[n] = p;
		n++;
	}
}

vector<Point> PointSet::getPoints()
{
	return points;
}


/*
 * lof.cpp
 *
 *  Created on: 08 Feb 2018
 *      Author: dylan
 */

#include "../headers/lof.hpp"

vector<float> LOF::calculate(size_t k)
{
	size_t i = 0;
	// -------------------------------------------------------
	// Create the output vector of LOFs
	// -------------------------------------------------------
	size_t N = points->size();
	vector<float> lof(N,0);

	// -------------------------------------------------------
	// Build the k-d tree
	// -------------------------------------------------------
	PointCloud<float> cloud;
	for(size_t i=0; i<N; i++) {
		Point p = points->at(i);
		cloud.insert(p.x, p.y, p.z);
	}
	kdtree_t tree(3, cloud, KDTreeSingleIndexAdaptorParams(10) );
	tree.buildIndex();

	// ------------------------------------------------------------
	// Get the neighbours of each point and the distances to those
	// neighbours.
	// ------------------------------------------------------------
	vector<vector<size_t> > nbrs(N);
	vector<vector<float> >  dists(N);
	getNeighbours(tree, k+1, &nbrs, &dists);

	// ------------------------------------------------------------
	// Determine the local outlier factors for each point.
	// ------------------------------------------------------------
	vector<float> kdist(N);
	vector<float> lrd(N);
	// Get the K-distances
	for(i=0; i<N; i++)
		kdist[i] = getKDist(&(dists[i]));
	// Get the local reachability distances
	for(i=0; i<N; i++)
		lrd[i] = getLRD(&(nbrs[i]), &(dists[i]), &kdist);

	for(i=0; i<N; i++)
		lof[i] = getLOF(&(nbrs[i]), &lrd, lrd[i]);

	return lof;
}

void LOF::getNeighbours(kdtree_t& tree, size_t k,
					    vector<vector<size_t> >* nbrs,
					    vector<vector<float> >*  dists)
{
	size_t N = points->size();
	float query_pt[3];
	Point P;
	for(size_t i=0; i<N; i++) {
		vector<size_t> ret_index(k);   //!> Indices of neighbours
		vector<float>  ret_dist_sq(k); //!> Squared distances to neighbours
		vector<float> ret_dist(k);     //!> Distances to neighbours

		P = points->at(i);             //!> Query point
		query_pt[0] = P.x;
		query_pt[1] = P.y;
		query_pt[2] = P.z;

		size_t num_results = tree.knnSearch(&query_pt[0], k,
											&ret_index[0],
											&ret_dist_sq[0]);
		ret_index.resize(num_results);
		ret_dist_sq.resize(num_results);
		// Square root the distances.
		for(size_t j=0; j<num_results; j++) {
			ret_dist[j] = sqrt(ret_dist_sq[j]);
		}

		(*nbrs)[i] = ret_index;
		(*dists)[i] = ret_dist;
	}
}

float LOF::getLRD(const vector<size_t>* nbr_inds,
			 	  const vector<float>*  nbr_dists,
			 	  const vector<float>*  kdists)
{
	float lrd_temp, lrd;
	size_t j, jnn_ind;

	lrd_temp = 0;
	for(j=1; j<nbr_inds->size(); j++) {
		jnn_ind = nbr_inds->at(j);
		lrd_temp = lrd_temp + max(kdists->at(jnn_ind), nbr_dists->at(j));
	}
	lrd = (float)nbr_inds->size()/lrd_temp;

	return lrd;
}

float LOF::getKDist(const vector<float>* nbr_dists)
{
	float max_d = 0;
	for(size_t i=0; i<nbr_dists->size(); i++) {
		if(nbr_dists->at(i) > max_d)
			max_d = nbr_dists->at(i);
	}
	return max_d;
}

float LOF::getLOF(const vector<size_t>* nbr_inds,
			 	  const vector<float>*  lrds,
				  float lrd_i)
{
	size_t j, jnn_ind;
	float lof_temp, lof;

	lof_temp = 0;
	for(j=1; j<nbr_inds->size(); j++) {
		jnn_ind = nbr_inds->at(j);
		lof_temp += lrds->at(jnn_ind);
	}
	lof = lof_temp / ( (float)nbr_inds->size() * lrd_i );


	return lof;
}



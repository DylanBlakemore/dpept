/*
 * frame.cpp
 *
 *  Created on: 09 Jan 2018
 *      Author: dylan
 */

#include "../headers/frame.hpp"

int Frame::addLine(const Line& line)
{
	if(count >= n_lines_)
		return -1;
	else
		lines[count] = line;

	count++;
	return 0;
}

void Frame::print()
{
	for(size_t i=0; i<lines.size(); i++) {
		lines[i].print();
	}
}

PointSet Frame::getPointSet(const Settings& settings)
{
	PointSet ps(lines.size());
	// ------------------------------------------------------------
	// Create vector containing all points that will be used in the Delaunay
	// triangulation.
	// ------------------------------------------------------------
	vector<FADE3D::Point3> P;
	vector<size_t> line_inds;
	float t;
	discretizeFrame(settings, &P, &line_inds, &t);


	// ------------------------------------------------------------
	// Perform the tessellation
	// ------------------------------------------------------------
	FADE3D::Fade_3D dt;
	vector<FADE3D::Point3*> handles;
	dt.insert(P, handles);

	// ------------------------------------------------------------
	// For each line, determine the point with the smallest Delaunay
	// volume around it. This is done by using the line_indices
	// vector to iterate over the points along a single line and
	// determining the volume of the tetrahedra.
	// ------------------------------------------------------------
	size_t ind, min_ind;
	float vol_sum, min_vol;
	ind = 0;

	for(size_t l=0; l<lines.size(); l++) {
		min_ind = ind;
		min_vol = 100000000;

		while(line_inds[ind] == l && ind < P.size()) {
			vol_sum = 0;
			vector<FADE3D::Tet3*> tets;
			// ------------------------------------------------------------
			// Gets the tetrahedra around the point at position ind
			// in the vector of all the points.
			// ------------------------------------------------------------
			dt.getTetsAroundVertex(handles[ind], tets);
			// ------------------------------------------------------------
			// Calculate the volume of the tetrahedra
			// ------------------------------------------------------------
			for(size_t t=0; t<tets.size(); t++) {
				vol_sum += tetVolume(tets[t]);
			}
			if(vol_sum < min_vol) {
				min_vol = vol_sum;
				min_ind = ind;
			}
			ind++;
		}
		Point p_new(P[min_ind].x(),
					P[min_ind].y(),
					P[min_ind].z(),
					t, min_vol);
		ps.insert(p_new);
	}
	return ps;
}

void Frame::discretizeFrame(const Settings& settings, vector<FADE3D::Point3>* P_out,
							vector<size_t>* inds_out, float* t_out)
{
	int t_tot = 0;
	for(size_t i=0; i<lines.size(); i++) {
		// Get the discretized points for this line
		vector<FADE3D::Point3> disc = lines[i].discretizeLine(settings.disc_length);
		// Add items to the vector of line indices
		P_out->insert(P_out->end(), disc.begin(), disc.end());
		for(size_t p=0; p<disc.size(); p++) {
			inds_out->push_back(i);
		}
		t_tot += (float)lines[i].getTime();
	}
	*t_out = (float)t_tot/(float)lines.size();
}

bool Frame::doSkip(const Settings& settings)
{
	bool skip = 0;

	float t1 = (float)lines[0].getTime();
	float t2 = (float)lines[lines.size()-1].getTime();

	if(t1 == t2)
		skip = 0;
	else if((float)lines.size()/(t2-t1) < settings.min_decay)
		skip = 1;

	return skip;
}


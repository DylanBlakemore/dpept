/*
 * locationfile.cpp
 *
 *  Created on: 26 Feb 2018
 *      Author: dylan
 */
#include "../headers/locationfile.hpp"

void LocationFile::init(const string& folder)
{
	string location_path = folder + "/locations.dsv";
	vector<string> location_lines = readLines(location_path, 1);
	for(size_t i = 0; i < location_lines.size(); i++) {
		vector<float> location = split(location_lines[i], '\t');
		locations.push_back(location);
	}

	string covariance_path = folder + "/covariance.dsv";
	vector<string> covariance_lines = readLines(covariance_path, 1);
	for(size_t i = 0; i < covariance_lines.size(); i++) {
		vector<float> covariance = split(covariance_lines[i], '\t');
		covariances.push_back(covariance);
	}

	position = 0;
}

vector<float> LocationFile::split(const string& s, char delim)
{
	vector<float> tokens;
	string token;
	std::istringstream token_stream(s);
	while(getline(token_stream, token, delim)) {
		tokens.push_back((float)strtod(token.c_str(), NULL));
	}
	return tokens;
}

vector<string> LocationFile::readLines(string& path, size_t header)
{
	vector<string> lines;
	string line;
	ifstream lfile(path.c_str());
	if(lfile.is_open()) {
		size_t line_num = 1;
		while(getline(lfile, line)) {
			if(line_num > header)
				lines.push_back(line);
			line_num++;
		}
	} else {
		cerr << "Unable to open file " << path << endl;
		cerr << "Tracking terminating." << endl;
		throw std::runtime_error("File does not exist.");
	}

	return lines;
}

int LocationFile::getNextMeasurementSet(vector<Measurement>* measurements, const Settings& settings)
{
	if(position >= locations.size())
		return 0;

	*measurements = vector<Measurement>(0);
	size_t frame_id = locations[position][5];
	while(locations[position][5] == frame_id) {
		vector<float> location   = locations[position];
		vector<float> covariance = covariances[position];
		Measurement measurement(location[0], location[1], location[2], location[3],
								(size_t)location[4], covariance);
		measurements->push_back(measurement);
		position++;
		if(position == locations.size())
			break;
	}

	return 1;
}

vector<Measurement> LocationFile::mergeSimilarMeasurements(vector<Measurement>& all, const Settings& settings)
{
	vector<Measurement> merged = all;
	bool no_merge = 0;
	while(!no_merge) {
		for(size_t i = 0; i < merged.size(); i++) {
			for(size_t j = 0; j < merged.size(); j++) {
				if(j == i) continue;

			}
		}
	}
	return merged;
}


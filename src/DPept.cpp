//============================================================================
// Name        : DPept.cpp
// Author      : Dylan Blakemore
// Version     : 0.1
// Copyright   : 
// Description : An adapted version of the VPept multiple particle tracking
// 				 software.
//============================================================================

#include <iostream>
#include <chrono>

#include <Point3.h>

#include "../headers/frame.hpp"
#include "../headers/dptools.hpp"
#include "../headers/lmfile.hpp"
#include "../headers/pointset.hpp"
#include "../threadpool/ThreadPool.h"
#include "../headers/location.hpp"
#include "../headers/workerinput.hpp"
#include "../headers/track.hpp"
#include "../headers/measurement.hpp"
#include "../headers/locationfile.hpp"
#include "../matrix/Matrix.h"

using namespace std;
using namespace std::chrono;

int performLocation(const Settings& settings);
Location threadWorker(WorkerInput input);

int performTracking(const Settings& settings);
vector<int> matchMeasurements(vector<Track>& tracks, vector<Measurement>& measurements, bool USE_ACC);
bool gateTest(Track& track, Measurement& measurement, const Settings& settings);
void terminateTracks(vector<Track>& current, vector<Track>& terminated, float time, const Settings& settings);
vector<Track> stitchTracks(vector<Track>& raw, const Settings& settings);

int main() {
	// Load settings
	string conf_path = "/home/dylan/workspace/C++/DPept/config2.ini";
	Settings settings(conf_path);
	createOutputFolder(settings);
	createOutputFiles(settings);
	high_resolution_clock::time_point t1 = high_resolution_clock::now();

	if(!settings.track_only) {
		if(!performLocation(settings))
			return -1;
	}

	high_resolution_clock::time_point t2 = high_resolution_clock::now();
	duration<double> time_span = duration_cast<duration<double>>(t2 - t1);
	cout << "Time taken for triangulation = " << time_span.count() << endl;

	cout << "Starting tracking." << endl;
	performTracking(settings);
	cout << "Tracking completed." << endl;

	cout << "Done." << endl;

	return 0;
}

/** @brief Uses Delaunay triangulation to locate tracers at each
 * 			successive time frame.
 *
 * For each frame of lines of response within a list mode file,
 * locate the tracers within the field of view of the camera.
 *
 * These locations are written to a file.
 */

int performLocation(const Settings& settings)
{
	string lm_path = settings.input_file;
	size_t frame_s = settings.n_lines * settings.n_tracers;
	// ------------------------------------------------------------
	// Load the list mode file. If there are any errors they
	// are written to the terminal.
	// Possible errors:
	// 		- File not found.
	// 		- Start time is too large.
	// ------------------------------------------------------------
	LMFile input;
	try {
		input = LMFile(lm_path, frame_s, settings.n_frames,
					   settings.start_time, settings.window);
	}
	catch(const exception& exc) {
		cerr << exc.what() << endl;
		return 0;
	}
	cout << "File loaded successfully.\n" << endl;

	// The vector used to store the frames of a batch.
	vector<Frame> batch(settings.n_frames);
	int ibatch = 0;
	size_t iframe = 0;
	// ------------------------------------------------------------
	// Process the specified number of frames.
	// ------------------------------------------------------------
	int batch_result = 0;
	while(ibatch < settings.n_batches || settings.n_batches == -1) {
		cout << "Batch " << ibatch+1 << endl;
		ThreadPool pool(settings.n_cores);
		vector< std::future<Location> > results;
		size_t i_nframes = 0;

		// ------------------------------------------------------------
		// Errors may be thrown if:
		// 		- File not found.
		// 		- The size of the batch vector does not match the
		// 		  number of frames per batch defined in the configuration
		// 		  file.
		// ------------------------------------------------------------
		try {
			batch_result = input.getNextBatch(&batch);
		}
		catch(const std::exception& exc) {
			cerr << exc.what() << endl;
			return 0;
		}
		// ------------------------------------------------------------
		// batch_result takes the following values:
		// 		-1 : The next batch would start outside the bounds of the file.
		// 		 0 : All frames successfully loaded.
		// 		 1 : Frames successfully loaded, but end of file reached before
		// 		 	 batch could be filled.
		// ------------------------------------------------------------
		if(batch_result == -1) {
			cout << "End of file reached, triangulation terminating." << endl;
			break;
		}
		// ------------------------------------------------------------
		// If the batch_result is 1, we have to determine how many frames have
		// actually been filled.
		// ------------------------------------------------------------
		if(batch_result == 1) {
			while(i_nframes < frame_s && batch[i_nframes].isFull())
				i_nframes++;
		} else {
			i_nframes = settings.n_frames;
		}

		// Ignore the stream output of the FADE3D triangulation.
		cout.setstate(ios_base::failbit);

		for(size_t i=0; i<i_nframes; i++) {
			WorkerInput input;
			input.settings = settings;
			input.frame = batch[i];
			input.frame_id = iframe;

			results.emplace_back(
				pool.enqueue([input] {
					return threadWorker(input);
				})
			);
			iframe++;
		}

		vector<Location> locs;
		for(auto && result: results) {
			locs.push_back(result.get());
		}

		// Open up stream output again.
		cout.clear();

		for(size_t l=0; l<locs.size(); l++) {
			int print_result = printLocations(locs[l], settings, '\t');
			if(!print_result) {
				cerr << "Could not write to output file. Process terminating." << endl;
				return -1;
			}
		}
		ibatch++;
		if(batch_result == 1)
			break;
	}

	return 1;
}

/** @brief Multiple target tracking algorithm.
 *
 * Performs the tracking part of the algorithm by reading in the
 * file generated by performLocation and using an adapted multiple
 * target tracking technique.
 */
int performTracking(const Settings& settings)
{
	LocationFile location_file(settings.output_folder);
	vector<Track> current_tracks(0);
	vector<Track> complete_tracks(0);
	vector<Measurement> measurements;

	bool USE_ACC = settings.use_acc;

	// ------------------------------------------------------------
	// Get the set of measurements at the next time step
	//-------------------------------------------------------------
	while (location_file.getNextMeasurementSet(&measurements, settings)) {
		vector<int> matches;
		// Create new tracks if there are none existing
		if(current_tracks.size() == 0) {
			for(size_t m_i = 0; m_i < measurements.size(); m_i++) {
				current_tracks.push_back(Track(measurements[m_i], USE_ACC));
			}
		} else {
			// Determine which tracks the measurements belong to
			matches = matchMeasurements(current_tracks, measurements, USE_ACC);
			for(size_t m_i = 0; m_i < measurements.size(); m_i++) {
				// Create a new track if the measurement was not matched
				if(matches[m_i] < 0) {
					current_tracks.push_back(Track(measurements[m_i], USE_ACC));
				} else {
					// Measurement has to fall within a predefined distance of a track
					// in order to be associated.
					bool gate_test = gateTest(current_tracks[matches[m_i]], measurements[m_i], settings);
					if(gate_test)
						current_tracks[matches[m_i]].update(measurements[m_i], settings.error_sigma);
					else
						current_tracks.push_back(Track(measurements[m_i], USE_ACC));
				}
			}
		}

		terminateTracks(current_tracks, complete_tracks,
						measurements[0].t, settings);
	}
	// -----------------------------------------------------------
	// Add all remaining tracks to complete_tracks
	// -----------------------------------------------------------
	for(size_t i = 0; i < current_tracks.size(); i++) {
		complete_tracks.push_back(current_tracks[i]);
	}
	// -----------------------------------------------------------
	// Write tracks to file.
	// -----------------------------------------------------------
	size_t track_id = 0;
	for(size_t i = 0; i < complete_tracks.size(); i++) {
		if(complete_tracks[i].states.size() >= settings.min_entries) {
			printTracks(complete_tracks[i], track_id, settings, '\t');
			track_id++;
		}
	}
	cout << track_id << " tracks found." << endl;
	return 0;
}

/** @brief Worker function to be used by the threads.
 *
 * The function passed to the threads to perform the triangulation
 * and clustering of the lines of response.
 */
Location threadWorker(WorkerInput input)
{
	Location loc;
	if(input.frame.doSkip(input.settings))
		return loc;

	PointSet ps = input.frame.getPointSet(input.settings);

	ps.cleanse(input.settings.lof_k,
			   input.settings.lof_frac,
			   input.settings.vol_frac);

	vector<Point> result = ps.cluster(input.settings.dbscan_minpts,
									  input.settings.dbscan_eps);

	loc.points = result;
	loc.id = input.frame_id;


	return loc;
}

vector<int> matchMeasurements(vector<Track>& tracks, vector<Measurement>& measurements, bool USE_ACC)
{
	size_t n_t = tracks.size();
	size_t n_m = measurements.size();
	vector<int> matches(n_m, -1); // The vector to return.
	float time = measurements[0].t;
	// The matrices which build the match matrix.
	vector<vector<bool> > A(n_t);
	vector<vector<bool> > B(n_t);
	// --------------------------------------------------------------------
	// Build the matrix with the distances between measurements and tracks
	//---------------------------------------------------------------------
	vector<vector<float> > distances(n_t);
	float x1[3];
	float x2[3];

	for(size_t i = 0; i < n_t; i++) {
		distances[i] = vector<float>(n_m);
		for(size_t j = 0; j < n_m; j++) {
			Matrix x1_m = tracks[i].predict(time);
			Matrix x2_m = measurements[j].x;
			// Assign values to arrays for distance calculation.
			for(size_t d = 0; d < 3; d++) {
				x1[d] = x1_m.getNumber(d+1, 1);
				x2[d] = x2_m.getNumber(d+1, 1);
			}
			distances[i][j] = sqrdist(x1, x2);
		}
	}
	// -------------------------------------------------------------------
	// Find the smallest distance per row.
	// -------------------------------------------------------------------
	for(size_t i = 0; i < n_t; i++) {
		A[i] = vector<bool>(n_m);
		// ---------------------------------------------------------------
		// Initialize B here as well, even though it's not used yet. This
		// is done because the structure of the loop which finds the smallest
		// distance per column does not facilitate it well.
		// ---------------------------------------------------------------
		B[i] = vector<bool>(n_m);
		float min_dist = distances[i][0];
		float min_ind  = 0;
		for(size_t j = 1; j < n_m; j++) {
			if(distances[i][j] < min_dist) {
				min_dist = distances[i][j];
				min_ind  = j;
			}
		}
		A[i][min_ind] = 1;
	}

	// -------------------------------------------------------------------
	// Find the smallest distance per column.
	// -------------------------------------------------------------------
	for(size_t j = 0; j < n_m; j++) {
		float min_dist = distances[0][j];
		float min_ind  = 0;
		for(size_t i = 1; i < n_t; i++) {
			if(distances[i][j] < min_dist) {
				min_dist = distances[i][j];
				min_ind  = i;
			}
		}
		B[min_ind][j] = 1;
	}

	// -------------------------------------------------------------------
	// Match a measurement with a track if the relevant index is both the
	// smallest distance in that column and row.
	// -------------------------------------------------------------------
	for(size_t i = 0; i < n_t; i++) {
		for(size_t j = 0; j < n_m; j++) {
			if(A[i][j] && B[i][j])
				matches[j] = i;
		}
	}

	return matches;
}

void terminateTracks(vector<Track>& current, vector<Track>& terminated, float time, const Settings& settings)
{
	size_t counter = 0;
	while(counter < current.size()) {
		float dt = time - current[counter].states.back().t;
		if(dt >= settings.max_dt) {
			terminated.push_back(current[counter]);
			current.erase(current.begin() + counter);
		} else {
			counter++;
		}
	}
}

bool gateTest(Track& track, Measurement& measurement, const Settings& settings)
{
	Matrix x1_m = track.predict(measurement.t);
	Matrix x2_m = measurement.x;
	float x1[3];
	float x2[3];

	for(size_t d = 0; d < 3; d++) {
		x1[d] = x1_m.getNumber(d+1, 1);
		x2[d] = x2_m.getNumber(d+1, 1);
	}

	float dist = sqrt(sqrdist(x1,x2));
	return (dist < settings.search_rad);
}

vector<Track> stitchTracks(vector<Track>& raw, const Settings& settings)
{
	vector<Track> stitched;

	while(raw.size() > 0) {
		size_t i = 1;
		size_t n_matches = 0;
		while(i < raw.size()) {
			if(raw[0].matches(raw[i], settings.max_overlap, settings.search_rad)) {
				raw[0].combine(raw[i]);
				raw.erase(raw.begin()+i);
				n_matches++;
			} else {
				i++;
			}
		}
		if(n_matches == 0) {
			stitched.push_back(raw[0]);
			raw.erase(raw.begin());
		}
	}

	return stitched;
}

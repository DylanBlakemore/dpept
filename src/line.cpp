/*
 * line.cpp
 *
 *  Created on: 09 Jan 2018
 *      Author: dylan
 */
#include <stdio.h>
#include <math.h>
#include "../headers/line.hpp"

void Line::print()
{
	printf("(%3.1f  %3.1f  %3.1f) ; ", A_[0], A_[1], A_[2]);
	printf("(%3.1f  %3.1f  %3.1f) t = %zi \n", B_[0], B_[1], B_[2], t_);
}

std::vector<FADE3D::Point3> Line::discretizeLine(float h)
{
	float L = sqrt(sqrdist(A_, B_));
	int n_p = ceil(L/h);
	std::vector<FADE3D::Point3> P(n_p+1);

	double x;
	double y;
	double z;

	for(int i=0; i<= n_p; i++) {
		x = (double) A_[0] + (double)i*(B_[0] - A_[0])/(double)n_p;
		y = (double) A_[1] + (double)i*(B_[1] - A_[1])/(double)n_p;
		z = (double) A_[2] + (double)i*(B_[2] - A_[2])/(double)n_p;
		P[i] = FADE3D::Point3(x,y,z);
	}

	return P;
}

size_t Line::getTime()
{
	return t_;
}

/*
 * track.cpp
 *
 *  Created on: 22 Feb 2018
 *      Author: dylan
 */

#include "../headers/track.hpp"

Track::Track(Measurement& initial, bool use_acc)
{
	USE_ACC = use_acc;
	P = factory.generateEstimateErrorMatrix(initial.R, USE_ACC);

	Matrix x_init(1,1);
	if(USE_ACC) {
		x_init.Resize(9,1);
		x_init.Clear();
		x_init.add(1,1,initial.x.getNumber(1,1));
		x_init.add(4,1,initial.x.getNumber(2,1));
		x_init.add(7,1,initial.x.getNumber(3,1));
	} else {
		x_init.Resize(6,1);
		x_init.Clear();
		x_init.add(1,1,initial.x.getNumber(1,1));
		x_init.add(3,1,initial.x.getNumber(2,1));
		x_init.add(5,1,initial.x.getNumber(3,1));
	}

	states.push_back(State(x_init, initial.t, initial.cluster_size));
}

Matrix Track::predict(float t)
{
	float dt = t - states.back().t;
	Matrix A = factory.generateSystemDynamicsMatrix(dt, USE_ACC);
	Matrix x_hat = A * states.back().x_hat;
	Matrix predicted(3,1);
	if(USE_ACC) {
		predicted.add(1,1,x_hat.getNumber(1,1));
		predicted.add(2,1,x_hat.getNumber(4,1));
		predicted.add(3,1,x_hat.getNumber(7,1));
	} else {
		predicted.add(1,1,x_hat.getNumber(1,1));
		predicted.add(2,1,x_hat.getNumber(3,1));
		predicted.add(3,1,x_hat.getNumber(5,1));
	}
	return predicted;
}

void Track::update(const Measurement& measurement, float sigma)
{
	float dt = measurement.t - states.back().t;
	Matrix A = factory.generateSystemDynamicsMatrix(dt, USE_ACC);
	Matrix At = mm.Transpose(A);
	Matrix C = factory.generateShapeMatrix(USE_ACC);
	Matrix Ct = mm.Transpose(C);
	Matrix Q = factory.generateDiscreteProcessNoise(dt, sigma, USE_ACC);
	Matrix R = measurement.R;
	Matrix y = measurement.x;
	Matrix I = mm.Eye(P.getRows());

	Matrix x_k = A * states.back().x_hat;
	P = A*P*At + Q;
	Matrix K = P*Ct * mm.Inv(C*P*Ct + R);
	x_k = x_k + K*(y - C*x_k);
	P = (I - K*C) * P;

	State new_state(x_k, measurement.t, measurement.cluster_size);
	states.push_back(new_state);
}

bool Track::matches(Track& t2, float max_overlap, float search_radius)
{
	bool does_match = 0;

	// t2 comes after the current track
	if(t2.states[0].t > this->states[0].t &&
	   t2.states.back().t > this->states.back().t) {

		float overlap = timeOverlap(*this, t2);
		if(overlap*overlap > max_overlap*max_overlap) {
			return 0;
		} else {
			float distance = trackDistance(*this, t2);
			if(distance < search_radius)
				does_match = 1;
		}
	}
	// The current track comes after t2
	else if(t2.states[0].t > this->states[0].t &&
			t2.states.back().t > this->states.back().t) {

		float overlap = timeOverlap(t2, *this);
		if(overlap*overlap > max_overlap*max_overlap) {
			return 0;
		} else {
			float distance = trackDistance(t2, *this);
			if(distance < search_radius)
				does_match = 1;
		}
	}
	// The tracks are not sequential
	else {
		return 0;
	}
	return does_match;
}

/** @brief Returns the amount of time by which two tracks overlap.
 *
 * If this number is positive, the second is after the first, with a gap between.
 * If it is negative, the two overlap.
 */
float Track::timeOverlap(Track& first, Track& second)
{
	return second.states[0].t - first.states.back().t;
}

float Track::trackDistance(Track& first, Track& second)
{
	// ---------------------------------------------------------
	// Perform a linear least squares fit for both tracks.
	// ---------------------------------------------------------
	Matrix predicted = first.predict(second.states.back().t);
	Matrix second_1(3,1);
	if(USE_ACC) {
		second_1.add(1,1,second.states[0].x_hat.getNumber(1,1));
		second_1.add(2,1,second.states[0].x_hat.getNumber(4,1));
		second_1.add(3,1,second.states[0].x_hat.getNumber(7,1));
	} else {
		second_1.add(1,1,second.states[0].x_hat.getNumber(1,1));
		second_1.add(2,1,second.states[0].x_hat.getNumber(3,1));
		second_1.add(3,1,second.states[0].x_hat.getNumber(5,1));
	}
	Matrix diff = second_1 - predicted;
	return sqrt(mm.dot(diff,diff));
}

void Track::combine(Track& t2)
{
	// The current track is first chronologically
	if(this->states[0].t < t2.states[0].t) {
		for(size_t i = 0; i < t2.states.size(); i++) {
			this->states.push_back(t2.states[i]);
		}
	} else {
		for(size_t i = 0; i < t2.states.size(); i++) {
			this->states.insert(this->states.begin(), t2.states[i]);
		}
	}
}



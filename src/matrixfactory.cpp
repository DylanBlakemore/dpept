/*
 * matrixfactory.cpp
 *
 *  Created on: 22 Feb 2018
 *      Author: dylan
 */
#include "../headers/matrixfactory.hpp"

Matrix MatrixFactory::generateShapeMatrix(bool USE_ACC)
{
	Matrix C(1,1);
	if(USE_ACC) {
		C.Resize(3,9);
		C.add(1,1,1.0);
		C.add(2,4,1.0);
		C.add(3,7,1.0);

	} else {
		C.Resize(3,6);
		C.add(1,1,1.0);
		C.add(2,3,1.0);
		C.add(3,5,1.0);
	}

	return C;
}

Matrix MatrixFactory::generateSystemDynamicsMatrix(float dt, bool USE_ACC)
{
	Matrix A(1,1);

	if(USE_ACC) {
		A.Resize(9,9);
		A.Clear();
		for(int i = 1; i <= 9; i++) {
			A.add(i,i,1);
		}

		A.add(1,2,dt);
		A.add(2,3,dt);
		A.add(4,5,dt);
		A.add(5,6,dt);
		A.add(7,8,dt);
		A.add(8,9,dt);

		A.add(1,3,0.5*dt*dt);
		A.add(4,6,0.5*dt*dt);
		A.add(7,9,0.5*dt*dt);
	} else {
		A.Resize(6,6);
		// Populate with zeros
		A.Clear();
		// Ones along the diagonal
		for(int i = 1; i <= 6; i++) {
			A.add(i,i,1);
		}
		// dt at correct positions
		A.add(1,2,dt);
		A.add(3,4,dt);
		A.add(5,6,dt);
	}

	return A;
}

Matrix MatrixFactory::generateDiscreteProcessNoise(float dt, float sigma, bool USE_ACC)
{
	Matrix Q(1,1);
	if(USE_ACC) {
		Q.Resize(9,9);
		Matrix q(3,3);
		q.add(1,1,0.25*dt*dt*dt*dt);
		q.add(1,2,0.5*dt*dt*dt);
		q.add(1,3,0.5*dt*dt);
		q.add(2,1,0.5*dt*dt*dt);
		q.add(2,2,dt*dt);
		q.add(2,3,dt);
		q.add(3,1,0.5*dt*dt);
		q.add(3,2,dt);
		q.add(3,3,1.0);
		for(int i = 0; i < 3; i++) {
			for(int j = 1; j <= 3; j++) {
				for(int k = 1; k <= 3; k++) {
					int x = j + 3*i;
					int y = k + 3*i;
					Q.add(x,y,q.getNumber(j,k));
				}
			}
		}
	} else {
		Q.Resize(6,6);
		Matrix q(2,2);
		q.add(1,1,dt*dt*dt*dt/4);
		q.add(1,2,dt*dt*dt/2);
		q.add(2,1,dt*dt*dt/2);
		q.add(2,2,dt*dt);
		for(int i = 0; i < 3; i++) {
			for(int j = 1; j <= 2; j++) {
				for(int k = 1; k <= 2; k++) {
					int x = j + 2*i;
					int y = k + 2*i;
					Q.add(x,y,q.getNumber(j,k));
				}
			}
		}
	}
	return Q*(sigma*sigma);
}

Matrix MatrixFactory::generateEstimateErrorMatrix(Matrix cov, bool USE_ACC)
{
	Matrix P(1,1);
	if(USE_ACC) {
		P.Resize(9,9);
		P.add(1,1,cov.getNumber(1,1));
		P.add(4,4,cov.getNumber(2,2));
		P.add(7,7,cov.getNumber(3,3));
		// Set velocity errors as large.
		P.add(2,2,50);
		P.add(5,5,50);
		P.add(8,8,50);
		// Set acceleration errors as large.
		P.add(3,3,50);
		P.add(6,6,50);
		P.add(9,9,50);
	} else {
		P.Resize(6,6);
		// Set the errors mapping x->x, y->y and z->z as the covariances
		P.add(1,1,cov.getNumber(1,1));
		P.add(3,3,cov.getNumber(2,2));
		P.add(5,5,cov.getNumber(3,3));
		// Set velocity errors as large.
		P.add(2,2,100);
		P.add(4,4,100);
		P.add(6,6,100);
	}
	return P;
}

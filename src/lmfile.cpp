/*
 * lmfile.cpp
 *
 *  Created on: 09 Jan 2018
 *      Author: dylan
 */

#include "../headers/lmfile.hpp"

/**
 * Reads a series of 4 chars into a word, which is then
 * converted into 6 floats which describe the locations
 * of two points A and B on a line such that
 * 		position[0] = A_x
 * 		position[1] = A_y
 * 		position[2] = A_z
 * 		position[3] = B_x
 * 		position[4] = B_y
 * 		position[5] = B_z
 */
void LMFile::decodeWord(const unsigned char* word, float* positions)
{
	unsigned int longword = word[3]+256*word[2]+256*256*word[1]+256*256*256*word[0];

	int sangle = longword & 511;
	int offset = (longword>>9) & 511;
	if(offset>255)offset = offset - 512;
	int ringA = (longword>>25) & 7;
	int ringB = (longword>>28) & 7;
	int planeA = (longword>>19) & 7;
	int planeB = (longword>>22) & 7;
	int i = abs(offset) % 2;
	int detA = (576 + (offset-i)/2 + sangle)%576;
	int detB = (288-(offset+i)/2 + sangle)%576;

	float thetaA = pi * detA/288;
	float thetaB = pi * detB/288;

	positions[0] = rad*sin(thetaA);
	positions[1] = rad*cos(thetaA);
	positions[2] = planeA*dplane+(ringA-3)*dring;
	positions[3] = rad*sin(thetaB);
	positions[4] = rad*cos(thetaB);
	positions[5] = planeB*dplane+(ringB-3)*dring;
}

/**
 * Gets the next batch of frames for processing.
 * Returns
 * 		-1 : Byte position of next frame is larger than file.
 * 		 0 : Import successfully completed.
 * 		 1 : Import successfully completed, and end of file reached.
 */

int LMFile::getNextBatch(vector<Frame>* batch)
{
	// ------------------------------------------------------------
	// The size of the batch vector should be equal to the
	// number of frames per batch.
	// ------------------------------------------------------------
	if(batch->size() != (unsigned int)n_frames_)
		throw std::runtime_error("Batch size mismatch.");

	FILE* input;
	input = fopen(f_path_.c_str(), "rb");
	// ------------------------------------------------------------
	// Throw error if file does not exist
	// ------------------------------------------------------------
	if(input == 0)
		throw std::runtime_error("Input list mode file does not exist.\n");

	// ------------------------------------------------------------
	// Sliding window can't have a window size of less than or equal to
	// zero or greater than the number of lines per frame.
	// ------------------------------------------------------------
	if(sw_size_ <= 0 || sw_size_ > n_lines_)
		sw_size_ = n_lines_;

	// ------------------------------------------------------------
	// Seek to correct position in file. Return -2 if that position
	// is greater than the end of the file.
	// ------------------------------------------------------------
	if(fseek(input, f_pos_, SEEK_SET)) {
		fclose(input);
		return -1;
	}

	bool eof = 0;			//<! Flags whether end of file is reached
	unsigned int idum;		//<! Counter used to extract chars from file
	unsigned char word[4];  //<! A line in raw data form
	long int t_pos = f_pos_;//<! Temporary file position place holder
	int t_time = time_;		//<! Temporary time place holder
	float positions[6];		//<! Describes a point int 3D
	// ------------------------------------------------------------
	// For each frame, extract the next lpf lines.
	// ------------------------------------------------------------
	for(size_t iframe=0; iframe < n_frames_; iframe++) {
		size_t iline = 0;
		(*batch)[iframe] = Frame(n_lines_);
		while(iline < n_lines_) {
			// End of file reached
			if(feof(input)) {
				eof = 1;
				break;
			}
			// Extract the next 4 bytes
			for(idum=0; idum<4; idum++)
				word[idum] = fgetc(input);
			f_pos_ += 4;

			// Increment time under this condition
			if(word[0]&128) {
				time_++;
				continue;
			}
			// Delayed coincidence - discard
			if(word[1]&4)
				continue;

			decodeWord(word, positions);
			// Create Line object and add to current frame
			Line line(positions, time_);
			(*batch)[iframe].addLine(line);

			iline++;

			// ------------------------------------------------------------
			// If the line has been reached at which the next frame will
			// start, save the byte position within the file.
			// ------------------------------------------------------------
			if(iline == sw_size_) {
				t_pos = f_pos_;
				t_time = time_;
			}
		}
		if(eof) break;

		// ------------------------------------------------------------
		// If end of file has been reached when trying to
		// seek to window position, break and flag eof
		// ------------------------------------------------------------
		if(fseek(input, t_pos, SEEK_SET)) {
			eof = 1;
			break;
		} else {
			// Set the current position and time to the temporary values
			f_pos_ = t_pos;
			time_ = t_time;
		}
	}
	fclose(input);
	return eof;
}

int LMFile::findStartByte(size_t start_t)
{
	FILE* input;
	input = fopen(f_path_.c_str(), "rb");
	// Return -1 if file does not exist.
	if(input == 0)
		return -1;

	unsigned int idum;
	unsigned char word[4];
	// ------------------------------------------------------------
	// Read through file until the specified time is reached.
	// ------------------------------------------------------------
	while(time_ < start_t) {
		// Read the next word.
		for(idum=0; idum<4; idum++)
			word[idum] = fgetc(input);
		// ------------------------------------------------------------
		// Return -2 if the end of the file is reached before
		// the time is found.
		// ------------------------------------------------------------
		if(feof(input)) {
			fclose(input);
			return -2;
		}

		f_pos_ += 4;
		// Increment the time.
		if(word[0]&128)
			time_ ++;
	}
	fclose(input);
	return 0;
}




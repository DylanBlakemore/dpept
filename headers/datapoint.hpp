/*
 * datapoint.hpp
 *
 *  Created on: 02 Mar 2018
 *      Author: dylan
 */

#ifndef DATAPOINT_HPP_
#define DATAPOINT_HPP_

class DataPoint
{
public:
	virtual ~DataPoint(){}
	virtual void getCoords(float xyz_out[3]) = 0;
	virtual void getTime(float* t_out) = 0;
	virtual void getClusterSize(size_t* c_size_out) = 0;
};



#endif /* DATAPOINT_HPP_ */

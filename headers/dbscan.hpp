/*
 * dbscan.hpp
 *
 *  Created on: 05 Feb 2018
 *      Author: bowbowbow
 *
 *     https://github.com/bowbowbow/DBSCAN
 */

#ifndef DBSCAN_HPP_
#define DBSCAN_HPP_

#include <Point3.h>
#include <cmath>
#include <vector>
#include "../nanoflann/nanoflann.hpp"
#include "../nanoflann/nanoutils.h"
#include "point.hpp"

using namespace std;

class DBSCAN
{
public:
	int minPts;
	float eps;
	vector<Point> points;
	int size;
	vector<vector<int> > adjPoints;
	vector<bool> visited;
	vector<vector<int> > cluster;
	int clusterIdx;

	/** @brief DBSCAN constructor.
	 *
	 * @param points A vector of Point objects
	 * @param minPts Integer, minimum number of points for cluster
	 * @param eps	 Float, maximum distance between points
	 */
	DBSCAN(const vector<Point>& points, int minPts, float eps) {
		this->eps = eps;
		this->minPts = minPts;
		this->points = points;
		this->size = (int)points.size();
		adjPoints.resize(size);
		this->clusterIdx=-1;
	}

	/** @brief Performs the clustering.
	 *
	 * Runs the DBSCAN clustering algorithm on the input points.
	 */
	void run () {
		checkNearPoints();

		for(int i=0;i<size;i++) {
			if(points[i].cluster != NOT_CLASSIFIED) continue;

			if(isCoreObject(i)) {
				dfs(i, ++clusterIdx);
			} else {
				points[i].cluster = NOISE;
			}
		}

		cluster.resize(clusterIdx+1);
		for(int i=0;i<size;i++) {
			if(points[i].cluster != NOISE) {
				cluster[points[i].cluster].push_back(i);
			}
		}
	}

	/** @brief Assigns cluster ID's to points.
	 *
	 * @param[in] now The ID of the current point.
	 * @param[in] c   The current cluster.
	 */
	void dfs (int now, int c) {
		points[now].cluster = c;
		if(!isCoreObject(now)) return;

		for(auto&next:adjPoints[now]) {
			if(points[next].cluster != NOT_CLASSIFIED) continue;
			dfs(next, c);
		}
	}
	/** @brief Checks for neighbours within eps distance.
	 *
	 * This is a brute force search method. However, for the DPept
	 * application, the number of points remaining after cleansing
	 * is low (normally much less than 1000) so a complexity of O(n^2)
	 * is not too much of an issue.
	 */
	void checkNearPoints() {
		for(int i=0;i<size;i++) {
			for(int j=0;j<size;j++) {
				if(i==j) continue;
				if(points[i].getDis(points[j]) <= eps) {
					points[i].pts_cnt++;
					adjPoints[i].push_back(j);
				}
			}
		}
	}

	/** @brief Checks whether a point is a core point.
	 *
	 * @param[in] The ID of the point to be checked.
	 * @return True or False
	 */
	bool isCoreObject(int idx) {
		return points[idx].pts_cnt >= minPts;
	}

	/** @brief Returns the idenified clusters.
	 *
	 * @return vector<vector<int> > Each vector<int> contains the indices
	 * 								of the points in a single cluster.
	 */
	vector<vector<int> > getCluster() {
		return cluster;
	}
};



#endif /* DBSCAN_HPP_ */

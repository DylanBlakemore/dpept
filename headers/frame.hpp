/*
 * frame.h
 *
 *  Created on: 08 Jan 2018
 *      Author: dylan
 *
 *  Class to represent a single frame of lines of response.
 */

#ifndef FRAME_HPP_
#define FRAME_HPP_

#include <vector>
#include <Fade_3D.h>

#include "dptools.hpp"
#include "line.hpp"
#include "pointset.hpp"

using namespace std;

class Frame {
public:
	Frame() {
		count = 0;
		n_lines_ = 0;
	}

	Frame(int n_lines) {
		lines = vector<Line>(n_lines);
		count = 0;
		n_lines_ = n_lines;
	}

	int addLine(const Line& line);
	void print();
	bool isFull(){return (count == n_lines_);}
	bool doSkip(const Settings& settings);
	PointSet getPointSet(const Settings& settings);
private:
	vector<Line> lines;
	size_t count;
	size_t n_lines_;

	void discretizeFrame(const Settings& settings, vector<FADE3D::Point3>* P_out,
						 vector<size_t>* inds_out, float* t_out);
};



#endif /* FRAME_HPP_ */

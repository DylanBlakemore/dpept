/*
 * pointset.hpp
 *
 *  Created on: 07 Feb 2018
 *      Author: dylan
 */

#ifndef POINTSET_HPP_
#define POINTSET_HPP_

#include "point.hpp"
#include "location.hpp"
#include "lof.hpp"
#include "quickselect.hpp"
#include "dbscan.hpp"
#include <vector>
using namespace std;

using namespace std;

class PointSet
{
public:
	PointSet(size_t N=0)
	{
		if(N > 0) points.resize(N);
		n = 0;
	}

	void cleanse(size_t k, float lof_frac, float vol_frac);
	void insert(const Point p);
	vector<Point> cluster(size_t k, float eps);
	vector<Point> getPoints();

private:
	void removeLarge(const vector<float>& metric, size_t N_e);

	vector<Point> points;
	size_t n;
};



#endif /* POINTSET_HPP_ */

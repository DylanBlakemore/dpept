/*
 * locationfile.hpp
 *
 *  Created on: 26 Feb 2018
 *      Author: dylan
 */

#ifndef LOCATIONFILE_HPP_
#define LOCATIONFILE_HPP_

#include "measurement.hpp"
#include "dptools.hpp"
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <stdexcept>

using namespace std;

class LocationFile
{
public:
	LocationFile(const string& folder) {init(folder);}

	int getNextMeasurementSet(vector<Measurement>* measurements, const Settings& settings);
private:
	vector<Measurement> mergeSimilarMeasurements(vector<Measurement>& all, const Settings& settings);
	void init(const string& folder);
	vector<string> readLines(string& path, size_t header=0);
	vector<float> split(const string& s, char delim);
	vector<vector<float> > locations;
	vector<vector<float> > covariances;
	size_t position;
};



#endif /* LOCATIONFILE_HPP_ */

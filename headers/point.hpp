/*
 * point.hpp
 *
 *  Created on: 07 Feb 2018
 *      Author: dylan
 */

#ifndef POINT_HPP_
#define POINT_HPP_

#include <math.h>
#include <vector>

using namespace std;

const int NOISE = -2;
const int NOT_CLASSIFIED = -1;

class Point
{
public:
	Point()
	{
		x = y = z = t = 0;
		vol = 0;
		cluster = NOT_CLASSIFIED;
		pts_cnt = 0;
		c_size = 0;
	}
	Point(float x_, float y_, float z_, float t_, float vol_=0)
	{
		x = x_;
		y = y_;
		z = z_;
		t = t_;
		vol = vol_;
		cluster = NOT_CLASSIFIED;
		pts_cnt = 0;
		c_size = 0;
	}

	float coord(size_t i) {
		if(i == 0)
			return x;
		if(i == 1)
			return y;
		if(i == 2)
			return z;
		else
			return 0;
	}

	float x, y, z, t;
	float vol;
	int cluster, pts_cnt;
	size_t c_size;
	float covariance[3][3];

	float getDis(const Point & ot) {
		return sqrt((x-ot.x)*(x-ot.x)+(y-ot.y)*(y-ot.y)+(z-ot.z)*(z-ot.z));
	}
};



#endif /* POINT_HPP_ */

/*
 * track.hpp
 *
 *  Created on: 22 Feb 2018
 *      Author: dylan
 */

#ifndef TRACK_HPP_
#define TRACK_HPP_

#include "../matrix/Matrix.h"
#include "../matrix/MatrixMath.h"
#include "matrixfactory.hpp"
#include "measurement.hpp"
#include "state.hpp"

#include <vector>
#include <iostream>

using namespace std;
using std::vector;

class Track
{
public:
	Track(){}
	Track(Measurement& initial, bool use_acc=0);
	/** @brief Predict the next position of the track
	 *
	 */
	Matrix predict(float t);
	/** Update the position using a Kalman filter.
	 *
	 */
	void update(const Measurement& measurement, float sigma=1);

	bool matches(Track& t2, float max_overlap, float search_radius);
	void combine(Track& t2);

	Matrix P; // the estimate error covariance
	vector<State> states;
private:
	bool USE_ACC;
	MatrixFactory factory;
	MatrixMath mm;
	float timeOverlap(Track& first, Track& second);
	float trackDistance(Track& first, Track& second);
};



#endif /* TRACK_HPP_ */

/*
 * lmfile.h
 *
 *  Created on: 08 Jan 2018
 *      Author: dylan
 *
 *  Class to represent a List Mode file (raw output from PEPT scanner).
 */

#ifndef LMFILE_HPP_
#define LMFILE_HPP_

#include <vector>
#include <cstdio>
#include <string>
#include <math.h>
#include <stdexcept>

#include "frame.hpp"

using namespace std;

class LMFile {
public:
	/*
	 * Constructor has default arguments for start time and
	 * sliding window size.
	 *
	 * Arguments
	 * 		f_path    : Path to the input list mode file.
	 * 		n_lines	  : The number of lines per frame.
	 * 		n_frames  : The number of frames per batch.
	 * 		start_t	  : The start time in milliseconds. Default = 0.
	 * 		sw_size   : The sliding window size. Default = no sliding window.
	 */
	LMFile(const string& f_path, size_t n_lines, size_t n_frames,
			size_t start_t=0, int sw_size=0)
	{
		init(f_path, n_lines, n_frames, start_t, sw_size);

		int result = findStartByte(start_t);

		if(result == -1)
			throw std::runtime_error("Input list mode file does not exist.\n");
		if(result == -2){
			cout << "Time = " << time_ << endl;
			cout << "File position = " << f_pos_ << endl;
			throw std::runtime_error("End of file reached before start time.\n");
		}
	}

	LMFile()
	{
		init("", 0,0,0,0);
	}

	int getNextBatch(vector<Frame>* batch);

private:
	std::string f_path_;	// The path to the listmode file.
	size_t f_pos_;// The current byte position within the file.
	size_t time_;		// The current time within the file
	size_t sw_size_;	// The sliding window size.
	size_t n_lines_;	// The number of lines of response per frame.
	size_t n_frames_;	// The number of frames to be read at a time;

	static constexpr float pi=3.14157, rad=420, dring=38.8, dplane=4.85;

	/*
	 * Finds the byte position in the file which corresponds
	 * to the required starting time.
	 */
	int findStartByte(size_t start_t);
	void decodeWord(const unsigned char* word, float* positions);

	void init(const string& f_path, size_t n_lines, size_t n_frames,
			  size_t start_t, size_t sw_size)
	{
		f_path_	  = f_path;
		time_    = 0;
		sw_size_  = sw_size;
		n_lines_  = n_lines;
		n_frames_ = n_frames;
		f_pos_ = 0;
	}
};



#endif /* LMFILE_HPP_ */

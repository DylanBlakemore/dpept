/*
 * dptools.h
 *
 *  Created on: 09 Jan 2018
 *      Author: dylan
 */

#ifndef DPTOOLS_HPP_
#define DPTOOLS_HPP_

#include <string.h>
#include <Point3.h>
#include <Vector3.h>
#include <Tet3.h>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <string>
#include "inireader.hpp"

#include "point.hpp"
#include "track.hpp"
#include "location.hpp"
#include "measurement.hpp"
#include "state.hpp"

using namespace std;

/** @brief Stores the settings from a config file.
 *
 * Uses the Boost ini_parser to parse the contents of an ini
 * configuration file.
 */
struct Settings {
	/** @brief Constructor
	 *
	 * @param f_path The full path to the config file.
	 */
	Settings(const string& f_path);
	Settings(){}

	// Important files
	string input_file,
		   output_folder,
		   config_path;
	bool track_only;
	// Input parameters
	size_t n_tracers,
		   n_lines,
		   n_cores,
		   n_frames,
		   start_time,
		   window;
	int n_batches;

	// Triangulation parameters
	float min_decay,
		  disc_length,
		  lof_frac,
		  vol_frac,
		  dbscan_eps;
	size_t lof_k, dbscan_minpts;

	// Tracking parameters
	size_t min_entries;
	bool use_acc;
	float max_dt,
		  search_rad,
		  max_overlap,
		  error_sigma;

};

/** @brief Prints a vector of points to a dsv file.
 *
 * @param[in] locs     A vector of Point objects.
 * @param[in] filename A string containing the path to the output file.
 * @param[in] delim    The delimiter to be used.
 *
 * @return int: 1 if successful, 0 if not.
 */
int printLocations(const Location& loc, const Settings& settings, char delim='\t');

/** @brief Calculates the Euclidean distance between two 3D points.
 *
 * Calculates the distance between two 3D points, represented
 * as arrays of floats with three elements.
 *
 * @param[in] A float[3], (x,y,z) coordinates of first point.
 * @param[in] B float[3], (x,y,z) coordinates of second point.
 *
 * @return float Eucidean distance.
 */
float sqrdist(const float* A, const float* B);

/** @brief Calculates the volume of a 3D tetrahedron.
 *
 * @param[in] tet FADE3D::Tet*
 *
 * @return float volume
 */
float tetVolume(const FADE3D::Tet3* tet);

int createFile(const string& path, const string& header);
int createOutputFiles(Settings& settings);
int printTracks(const Track& track, size_t id,  const Settings& settings, char delim);
int createOutputFolder(Settings& settings);

float measurementDistance(Measurement& m1, Measurement& m2);

#endif /* DPTOOLS_HPP_ */

/*
 * workerinput.hpp
 *
 *  Created on: 13 Feb 2018
 *      Author: dylan
 */

#ifndef WORKERINPUT_HPP_
#define WORKERINPUT_HPP_

#include "frame.hpp"
#include "dptools.hpp"

struct WorkerInput
{
	Frame frame;
	Settings settings;
	size_t frame_id;
};



#endif /* WORKERINPUT_HPP_ */

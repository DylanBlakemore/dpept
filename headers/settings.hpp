/*
 * settings.h
 *
 *  Created on: 02 Mar 2018
 *      Author: dylan
 */

#ifndef SETTINGS_HPP_
#define SETTINGS_HPP_

using namespace std;

/** @brief Stores the settings from a config file.
 *
 * Uses the Boost ini_parser to parse the contents of an ini
 * configuration file.
 */
struct Settings {
	/** @brief Constructor
	 *
	 * @param f_path The full path to the config file.
	 */
	Settings(const string& f_path);
	Settings(){}

	// Important files
	string input_file,
		   output_folder,
		   config_path;
	bool track_only;
	// Input parameters
	size_t n_tracers,
		   n_lines,
		   n_cores,
		   n_frames,
		   start_time,
		   window;
	int n_batches;

	// Triangulation parameters
	float min_decay,
		  disc_length,
		  lof_frac,
		  vol_frac,
		  dbscan_eps;
	size_t lof_k, dbscan_minpts;

	// Tracking parameters
	size_t min_entries;
	bool use_acc;
	float max_dt,
		  search_rad,
		  max_overlap,
		  error_sigma;

};



#endif /* SETTINGS_HPP_ */

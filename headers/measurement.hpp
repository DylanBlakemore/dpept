/*
 * measurement.hpp
 *
 *  Created on: 22 Feb 2018
 *      Author: dylan
 */

#ifndef MEASUREMENT_HPP_
#define MEASUREMENT_HPP_

#include "../matrix/Matrix.h"
#include "matrixfactory.hpp"

class Measurement
{
public:
	Measurement();
	Measurement(float x, float y, float z, float t_in,
			    size_t csize, const vector<float>& cov);

	Matrix x;
	Matrix R;
	float t;
	size_t cluster_size;

private:
	void init();
};



#endif /* MEASUREMENT_HPP_ */

/*
 * state.hpp
 *
 *  Created on: 22 Feb 2018
 *      Author: dylan
 */

#ifndef STATE_HPP_
#define STATE_HPP_

#include "../matrix/Matrix.h"
#include "../matrix/MatrixMath.h"

class State
{
public:
	State()
	{
		x_hat = Matrix(6,1);
		cluster_size = 0;
		t = 0;
	}

	State(Matrix x, float time, size_t cs)
	{
		x_hat = x;
		cluster_size = cs;
		t = time;
	}

	Matrix x_hat;
	float t;
	size_t cluster_size;
};



#endif /* STATE_HPP_ */

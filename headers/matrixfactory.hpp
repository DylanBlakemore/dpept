/*
 * matrixfactory.hpp
 *
 *  Created on: 22 Feb 2018
 *      Author: dylan
 */

#ifndef MATRIXFACTORY_HPP_
#define MATRIXFACTORY_HPP_

#include "../matrix/Matrix.h"
#include "../matrix/MatrixMath.h"

struct MatrixFactory
{
	Matrix generateSystemDynamicsMatrix(float dt, bool USE_ACC = 0);
	Matrix generateShapeMatrix(bool USE_ACC = 0);
	Matrix generateDiscreteProcessNoise(float dt, float sigma = 1, bool USE_ACC = 0);
	Matrix generateEstimateErrorMatrix(Matrix cov, bool USE_ACC = 0);
};



#endif /* MATRIXFACTORY_HPP_ */

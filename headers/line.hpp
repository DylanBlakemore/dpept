/*
 * lor.h
 *
 *  Created on: 08 Jan 2018
 *      Author: dylan
 *
 *  Class to represent a single line of response.
 */

#ifndef LINE_HPP_
#define LINE_HPP_

#include <vector>

#include <Point3.h>

#include "dptools.hpp"

class Line
{
public:
	Line()
	{
		for(int i=0; i<3; i++) {
			A_[i] = 0;
			B_[i] = 0;
		}
		t_ = 0;
		is_init = false;
	}

	Line(const float* A, const float* B, size_t t)
	{
		for(int i=0; i<3; i++) {
			A_[i] = A[i];
			B_[i] = B[i];
		}
		t_ = t;
		is_init = true;
	}

	Line(const float* AB, size_t t)
	{
		for(int i=0; i<3; i++) {
			A_[i] = AB[i];
			B_[i] = AB[i+3];
		}
		t_ = t;
		is_init = true;
	}

	Line(const Line & line)
	{
		for(int i=0; i<3; i++) {
			A_[i] = line.A_[i];
			B_[i] = line.B_[i];
		}
		t_ = line.t_;
		is_init = true;
	}

	~Line(){}

	std::vector<FADE3D::Point3> discretizeLine(float h);
	bool isInit(){return is_init;}
	size_t getTime();
	void print();
private:
	/* End points of the line of response. */
	float A_[3];
	float B_[3];
	size_t t_;
	bool is_init;
};



#endif /* LINE_HPP_ */

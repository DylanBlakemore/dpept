/*
 * quickselect.hpp
 *
 *  Created on: 08 Feb 2018
 *      Author: dylan
 */

#ifndef QUICKSELECT_HPP_
#define QUICKSELECT_HPP_

#include <vector>
using std::vector;

inline size_t partition(vector<float>& input, size_t p, size_t r)
{
	float pivot = input[r];

	while(p < r) {
		while(input[p] < pivot)
			p++;

		while(input[r] > pivot)
			r--;

		if(input[p] == input[r])
			p++;
		else if (p < r) {
			float tmp = input[p];
			input[p] = input[r];
			input[r] = tmp;
		}
	}

	return r;
}

inline float quickSelect(vector<float>& input, size_t p, size_t r, size_t k)
{
	if(p == r) return input[p];
	size_t j = partition(input, p, r);
	size_t length = j - p + 1;
	if(length == k) return input[j];
	else if(k < length) return quickSelect(input, p, j-1, k);
	else return quickSelect(input, j+1, r, k-length);
}

inline float getKthSmallest(vector<float> A, size_t k)
{
	size_t p = 0;
	size_t r = A.size()-1;
	return quickSelect(A, p, r, k);
}

#endif /* QUICKSELECT_HPP_ */

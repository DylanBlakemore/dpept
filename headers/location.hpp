/*
 * location.hpp
 *
 *  Created on: 07 Feb 2018
 *      Author: dylan
 */

#ifndef LOCATION_HPP_
#define LOCATION_HPP_

#include "point.hpp"
#include <vector>
using namespace std;

struct Location
{
	vector<Point> points;
	size_t id;
};

struct Cluster
{
	vector<Point> points;
	Point resolveLocation();
};

#endif /* LOCATION_HPP_ */

/*
 * lof.hpp
 *
 *  Created on: 08 Feb 2018
 *      Author: dylan
 */

#ifndef LOF_HPP_
#define LOF_HPP_

#include <vector>
#include "point.hpp"

#include "../nanoflann/nanoflann.hpp"
#include "../nanoflann/nanoutils.h"

using namespace nanoflann;
using namespace std;

typedef KDTreeSingleIndexAdaptor<
			L2_Simple_Adaptor<float, PointCloud<float> > ,
			PointCloud<float>,
			3
			> kdtree_t;

class LOF
{
public:
	LOF(vector<Point>* points_in)
	{
		points = points_in;
	}

	vector<float> calculate(size_t k);

private:
	void getNeighbours(kdtree_t& tree, size_t k,
					   vector<vector<size_t> >* nbrs,
					   vector<vector<float> >*  dists);

	float getLRD(const vector<size_t>* nbr_inds,
				 const vector<float>*  nbr_dists,
				 const vector<float>*  kdists);

	float getKDist(const vector<float>* nbr_dists);

	float getLOF(const vector<size_t>* nbr_inds,
				 const vector<float>*  lrds,
				 float lrd_i);

	vector<Point>* points;
};



#endif /* LOF_HPP_ */
